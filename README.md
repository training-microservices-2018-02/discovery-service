# Discovery Service #

Aplikasi untuk mendata semua service, nama dan lokasi (IP dan Port).

## Cara Deploy ##

1. Buat dua aplikasi di Heroku, di region `us` dan `eu`

        heroku apps:create --region eu tms-discovery-eu
        heroku apps:create --region us tms-discovery-us

2. Set environment variable

        heroku config:set SPRING_PROFILES_ACTIVE=heroku,us -a tms-discovery-us
        heroku config:set SPRING_PROFILES_ACTIVE=heroku,eu -a tms-discovery-eu
        heroku labs:enable runtime-dyno-metadata -a tms-discovery-us
        heroku labs:enable runtime-dyno-metadata -a tms-discovery-eu

3. Deploy ke heroku dengan CI

4. Pantau logs

        heroku logs --tail -a tms-discovery-us